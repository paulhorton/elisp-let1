;;; let1-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton.

;; License GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231002
;; Updated: 20231002
;; Version: 0.0
;; Keywords: let1

;;; Commentary:

;;; Change Log:

;;; Code:

(require 'let1)


(ert-deftest let1/test01 ()
  (should
   (=
    7
    (let1 x 3
      (+ x 4)
      )
    )))


(ert-deftest let1/test02 ()
  (should
   (=
    9
    (let1 x 3
      (setq x (+ 2 x))
      (+ x 4)
    )
    )))


(ert-deftest while-let1/test01 ()
  (should
   (equal
    '(25 16 9)
    (let ((data  '(3 4 5))
          squares
          )
      (while-let1 x (pop data)
        (push (* x x) squares)
        )
      squares
      ))))



;;; let1-tests.el ends here
