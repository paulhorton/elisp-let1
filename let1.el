;;; let1.el ---   Single variable let with fewer parens    -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2023 Paul Horton.

;; License GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210114
;; Updated: 20231002
;; Version: 0.0
;; Keywords: let1, fewer parens

;;; Commentary:

;; Based on an idea I found on the deflisp website,
;; let1 is a simple wrapper around let.

;; Compare:
;;   (let1  var  init-value
;;      (body-sexp1)
;;      (body-sexp2)
;;      ...
;;      )
;;
;;   (let ((var init-value))
;;      (body-sexp1)
;;      (body-sexp1)
;;      ...
;;      )

;; The two are equivalent, but let1 uses fewer parens.
;; I think it makes code slightly easier to read.
;; A minor side benefit is that let1 can check that you gave it at least 3 arguments.

;; when-let1, if-let1 are also provided as analogs to when-let and if-let
;;
;;

;;; Change Log:

;; 20240419: flet1 added as a shortcut for cl-flet
;;           Interface still tentative.

;;; Code:


(eval-when-compile (require 'subr-x)); for if-let


(defmacro let1 (name val &rest body)
  ;; Modified from deflisp website 2020.
  "Shortcut for (let ((NAME VAL)) BODY)"
  (declare (indent 2)
           (debug (symbolp form body))
           )
  (cl-assert name t "let1 error; attempted to use nil as NAME")
  ;; The following warning used to use cl-assert to throw an error,
  ;; but that caused problems when, while typing a let1 expression, compilation-at-point tried to expand the let1 to peek at variable names.
  (if (length< body 1)
      (lwarn 'emacs :warning "let1 warning; attempting to give variable '%s' an empty body makes no sense." name)
      )
  (if (eq nil val)
      (lwarn 'emacs :warning "let1 warning; assigning variable '%s' a nil value would be better writen using plain let." name)
      )
  `(let ((,name ,val))
    ,@body
    ))



(defmacro when-let1 (name val &rest body)
  "Shortcut for (when-let ((NAME VAL)) BODY)"
  (declare (indent 2)
           (debug (symbolp form body)))
  (cl-assert (consp (car-safe body)) t "when-let1 macro with nil or atom body makes no sense")
  `(let ((,name ,val))
     (when ,name ,@body)
     ))



(defmacro if-let1 (name val-form then-form &rest else-body)
  "eval VAL-FORM and assign it to NAME.
If the value was true return THEN-FORM,
otherwise eval ELSE-BODY like standard `if'.
"
  (declare
   (indent if-let1/indentor)
   (debug (symbolp form form body))
   )
  `(let ((,name ,val-form))
     (if ,name
         ,then-form
       ,@else-body
       )))


(defmacro while-let1 (name test-val &rest body)
  "Shortcut for (while-let ((NAME TEST-VAL)) BODY),
see `while-let' for more."
  (declare (indent 2)
           (debug (symbolp form body)))
  `(let (,name)
     (while (setq ,name ,test-val)
       ,@body
       )))


(defmacro ifnot-let1 (name val-form then-form &rest else-body)
  "Like `if-let1', except the flow is as (not VAL-FORM).
The THEN-FORM is used when VAL-FORM evaluates false,

Otherwise ELSE-BODY is evaluated with NAME bound to the
(true value) that VAL-FORM evaluated to."
  (declare
   (indent if-let1/indentor)
   (debug (symbolp form form body))
   )
  `(let ((,name ,val-form))
     (if (not ,name)
         ,then-form
       ,@else-body
       )))




;; ──────────  Functions for indentation  ──────────

(defun if-let1/indentor (pos state)
  "Function to compute indentation for if-let1."
  (ignore pos); Silence compiler warning about pos not being used.
  (let (
        (beg   (car (last (nth 9 state))))
        (end              (nth 2 state) )
        )
    (let  ((base-column  (save-excursion
                           (goto-char beg)
                           (current-column)
                           ))
           (count  (if-let1/indentor/how-many-sexps beg end))
           )
      (+ base-column (* lisp-body-indent
                        (if (= 2 count) 2 1)
                        )))))


(defun if-let1/indentor/how-many-sexps (beg end)
  "Number of sexps found to position BEG, when looking back from position END."
  ;; Warning, not well tested and the author is not familiar enough with lisp parsing to be confident of correctness, PH20221120
  (save-excursion
    (let1  count  0
      (goto-char end)
      (while (and (< beg (point))
                  (condition-case
                      nil
                      (progn (forward-sexp -1) (setq count (1+ count)))
                    (scan-error nil)
                    )))
      count
      )))



(defmacro flet1 (fun-name fun-arglist fun-expression &rest scope-body)
  "Wrapper around `cl-flet' for defining only one local function.
Compare:

   (cl-flet ((dub-x (val)
                    (* 2 val)
                    ))
    (dub-x x)
    ))

with

  (flet1 dub-x (val)
      (* 2 val)
    (dub-x x)
    ))
"
  (declare (indent if-let1/indentor)
           (debug (&define symbolp lambda-list form body))
           )
  (cl-assert fun-name t "flet1 error; attempted to use nil as NAME")
  (or (consp (car-safe scope-body))
      (lwarn 'emacs :warning "flet1 warning; attempting to give local function '%s' a nil value or atom body makes no sense" fun-name)
      )
  `(cl-flet ((,fun-name ,fun-arglist ,fun-expression)) ,@scope-body)
  )



(provide 'let1)

;;; let1.el ends here
