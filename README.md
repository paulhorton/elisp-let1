let1 is a simple wrapper around let.

Compare:
```
  (let1 var init-value BODY)
  (let ((var init-value)) BODY)
```

It has the simple advantage of slightly reducing visual clutter (())
and allows for more context aware error messages when malformed.


`if-let1`, `when-let1`, and `while-let1` are also provided.
